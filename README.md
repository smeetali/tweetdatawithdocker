# TweetDataWithDocker
pre-requsite : docker and minikube installed 


Command to create docker image

    docker build -t my-python-apps .

command to run docker image 
 
    docker run --network="host" -p 8000:5000 -it my-python-apps

command to start minikube in cmd 

    minikube start
   
    minikube docker-env

    @FOR /f "tokens=*" %i IN ('minikube -p minikube docker-env --shell cmd') DO @%i

OR

start minikube in shell

    minikube start
   
    minikube docker-env

    minikube -p minikube docker-env --shell powershell | Invoke-Expression


create kubectl yaml file containing docker image information that we need to deploy on minikube

run yaml file using cmd

    kubectl create deployment <pod-name> --image=<image-name>:<tag-name> 

    kubectl expose deployment <pod-name> --type=LoadBalancer --port=80

check pods using

    kubectl get pods

result will be as below :

NAME                           READY   STATUS             RESTARTS   AGE
test-image-7dd5dc7c99-cltm2    0/1     ImagePullBackOff   0          69m
test-images-64c9f995d4-2rspb   0/1     ImagePullBackOff   0          50m
twitter-hbc6h                  0/1     Completed          0          21m

